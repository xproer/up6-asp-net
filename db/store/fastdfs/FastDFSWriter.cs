﻿using System.IO;
using up6.db.model;
using up6.db.utils;
using up6.utils;

namespace up6.db.store
{
    /// <summary>
    /// FastDFS块写入器
    /// </summary>
    public class FastDFSWriter : FileBlockWriter
    {
        public FastDFSWriter() {
            this.storage = StorageType.FastDFS;
        }

        /// <summary>
        /// 创建文件，并返回文件ID
        /// </summary>
        /// <param name="pathSvr"></param>
        /// <param name="len"></param>
        public override string make(FileInf file)
        {
            byte[] data = new byte[0];
            var ext = FileTool.extension(file.pathSvr);
            return FastDFSTool.upload(data, ext);
        }

        public override string write(FileInf file, Stream stm)
        {
            byte[] data = new byte[stm.Length];
            stm.Seek(0, SeekOrigin.Begin);
            stm.Read(data, 0, data.Length);
            stm.Close();

            FastDFSTool.append(file.object_id, data);
            return string.Empty;
        }
    }
}