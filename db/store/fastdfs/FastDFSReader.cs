﻿namespace up6.db.store
{
    /// <summary>
    /// FastDFS块读取器
    /// </summary>
    public class FastDFSReader : FileBlockReader
    {
        public override byte[] read(string fileName, long offset, long size)
        {
            return FastDFSTool.down(fileName, offset, size);
        }
    }
}
