﻿using FastDFS.Client;
using FastDFS.Client.Common;
using FastDFS.Client.Config;
using FastDFS.Client.Storage;
using System.IO;
using up6.utils;

namespace up6.db.store
{
    /// <summary>
    /// FastDFS工具类
    /// </summary>
    public class FastDFSTool
    {
        public FastDFSTool() { }

        public static string upload(byte[] contentByte, string fileExt)
        {
            FastDFSTool fdt = new FastDFSTool();
            return FastDFSClient.UploadAppenderFile(fdt.node(), contentByte, fileExt);
        }

        public static string upload(Stream stm, string fileName)
        {
            FastDFSTool fdt = new FastDFSTool();
            byte[] data = new byte[stm.Length];
            stm.Read(data, 0, data.Length);
            return FastDFSClient.UploadFile(fdt.node(), data, fileName);
        }

        public static void remove(string fileName)
        {
            FastDFSTool fdt = new FastDFSTool();
            FastDFSClient.RemoveFile(fdt.node().GroupName, fileName);
        }

        public static void append(string fileName,byte[] data)
        {
            FastDFSTool fdt = new FastDFSTool();
            FastDFSClient.AppendFile(fdt.node().GroupName, fileName, data);
        }

        public static byte[] down(string fileName)
        {
            FastDFSTool fdt = new FastDFSTool();
            return FastDFSClient.DownloadFile(fdt.node(), fileName);
        }

        public static byte[] down(string fileName,long offset,long size)
        {
            Stream stm = new MemoryStream();

            FastDFSTool fdt = new FastDFSTool();
            long len = 1024;
            while (len>0)
            {
                var buf = FastDFSClient.DownloadFile(fdt.node(), fileName, offset, len);
                stm.Write(buf, 0, (int)len);
                offset+=len;
                len=(size-offset)>len ? len : (size-offset);
            }

            byte[] data = new byte[size];
            stm.Seek(0, SeekOrigin.Begin);
            stm.Read(data, 0, data.Length);
            stm.Close();
            return data;
        }

        public static FDFSFileInfo query(string fileName)
        {
            FastDFSTool fdt = new FastDFSTool();
            return FastDFSClient.GetFileInfo(fdt.node(), fileName);
        }
        

        public FastDfsConfig server()
        {
            ConfigReader cr = new ConfigReader();
            var root = cr.m_files.SelectToken("$.FastDFS");
            var ip = root["ip"].ToString().Trim();
            var port = int.Parse( root["port"].ToString().Trim());
            var group = root["group"].ToString().Trim();
            FastDfsConfig cfg = new FastDfsConfig();
            cfg.FastDfsServer.Add(new FastDfsServer { IpAddress=ip, Port=port });
            cfg.GroupName=group;
            return cfg;
        }

        public StorageNode node()
        {
            var svr = this.server();
            ConnectionManager.InitializeForConfigSection(svr);
            return FastDFSClient.GetStorageNode(svr.GroupName);
        }
    }
}