﻿using up6.db.utils;

namespace up6.db.store
{
    public class MinioReader : FileBlockReader
    {
        public override byte[] read(string fileName, long offset, long size)
        {
            fileName = PathTool.url_decode(fileName);
            return MinioTool.getObject(fileName, offset, size);
        }
    }
}