﻿using up6.utils;

namespace up6.db.store
{
    public class MinioConfig
    {
        public string ak = "minioadmin";
        public string sk = "minioadmin";
        public string service = "s3";
        public string algorithm = "AWS4-HMAC-SHA256";
        public string region = "us-east-1";
        public string endpoint = "192.168.0.111:9000";
        public string bucket = "test";

        public MinioConfig()
        {
            ConfigReader cr = new ConfigReader();
            this.ak = cr.readString("Minio.ak");
            this.sk = cr.readString("Minio.sk");
            this.region = cr.readString("Minio.region");
            this.endpoint = cr.readString("Minio.endpoint");
            this.bucket = cr.readString("Minio.bucket");
        }
    }
}