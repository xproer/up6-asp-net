﻿using System.IO;
using up6.db.model;
using up6.db.utils;
using up6.utils;

namespace up6.db.store
{
    public class MinioWriter : FileBlockWriter
    {
        public MinioWriter()
        {
            this.storage = StorageType.Minio;
        }

        /**
         * 生成UploadId
         */
        public override string make(FileInf file)
        {
            string ext = PathTool.getExtention(file.pathSvr);
            byte[] buf = new byte[0];
            string fileID = MinioTool.CreateMultipartUpload(file.S3Key());
            return fileID;
        }

        public override string write(FileInf file, Stream stm)
        {
            byte[] data = new byte[stm.Length];
            stm.Seek(0, SeekOrigin.Begin);
            stm.Read(data, 0, data.Length);
            stm.Close();
            stm = null;

            var etag = MinioTool.UploadPart(file.S3Key(), file.blockIndex, file.object_id, data);
            file.etag = etag;
            file.saveEtags();
            return etag;
        }

        public override bool writeLastPart(FileInf file)
        {
            return MinioTool.CompleteMultipartUpload(file);
        }
    }
}