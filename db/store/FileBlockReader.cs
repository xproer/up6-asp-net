﻿using System.IO;

namespace up6.db.store
{
    /// <summary>
    /// 文件块读取器
    /// </summary>
    public class FileBlockReader
    {
        public virtual byte[] read(string filePath,long offset,long size)
        {
            FileStream fs = new  FileStream(filePath, FileMode.Open, FileAccess.Read,FileShare.Read);
            byte[] buffer = new byte[size];
            fs.Seek(offset, SeekOrigin.Begin);
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();
            return buffer;
        }
    }
}