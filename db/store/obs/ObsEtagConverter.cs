﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OBS.Model;

namespace up6.db.store.obs
{
    public class ObsEtagConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var obj = serializer.Deserialize<UploadPartResponse>(reader);
            return obj;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var mod = value as UploadPartResponse;
            var obj = new JObject {
                { "ETag", mod.ETag.Replace("\"", "") },
                {"PartNumber",mod.PartNumber }
            };
            //writer.WriteValue(obj.ToString());
            obj.WriteTo(writer);
        }
    }
}