﻿using System.IO;
using up6.db.model;
using up6.db.utils;
using up6.utils;

namespace up6.db.store.obs
{
    public class OBSWriter : FileBlockWriter
    {
        public OBSWriter()
        {
            this.storage = StorageType.OBS;
        }

        /**
         * 生成UploadId
         */
        public override string make(FileInf file)
        {
            string ext = PathTool.getExtention(file.pathSvr);
            byte[] buf = new byte[0];
            string fileID = OBSTool.CreateMultipartUpload(file.ossKey());
            return fileID;
        }

        public override string write(FileInf file, Stream stm)
        {
            stm.Seek(0,SeekOrigin.Begin);//定位到开始位置
            var etag = OBSTool.uploadPart(file,stm);
            file.etag = etag;
            file.saveEtags();//保存
            return etag;
        }

        public override bool writeLastPart(FileInf file)
        {
            return OBSTool.CompleteMultipartUpload(file);
        }
    }
}