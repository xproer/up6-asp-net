﻿using up6.utils;

namespace up6.db.store.obs
{
    public class OBSConfig
    {
        public string ak = "minioadmin";
        public string sk = "minioadmin";
        public string endpoint = "192.168.0.111:9000";
        public string bucket = "test";

        public OBSConfig()
        {
            ConfigReader cr = new ConfigReader();
            this.ak = cr.readString("$.OBS.ak");
            this.sk = cr.readString("$.OBS.sk");
            this.endpoint = cr.readString("$.OBS.endpoint");
            this.bucket = cr.readString("$.OBS.bucket");
        }
    }
}