﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using up6.db.utils;

namespace up6.db.store.obs
{
    public class ObsEtagResolver: DefaultContractResolver
    {
        protected IEnumerable<string> propers;
        public ObsEtagResolver(IEnumerable<string> excludedProperties)
        {
            this.propers = excludedProperties;
        }
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            IList<JsonProperty> propertyList = base.CreateProperties(type, memberSerialization);

            var dic = UtilsTool.toDic(this.propers.ToArray());

            //仅序列化首字母相匹配的成员
            propertyList = propertyList.Where(p => dic.ContainsKey(p.PropertyName)).ToList();

            return propertyList;
        }
    }
}