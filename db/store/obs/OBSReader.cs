﻿using up6.db.utils;

namespace up6.db.store.obs
{
    public class OBSReader : FileBlockReader
    {
        public override byte[] read(string fileName, long offset, long size)
        {
            fileName = PathTool.url_decode(fileName);
            return OBSTool.getObject(fileName, offset, size);
        }
    }
}