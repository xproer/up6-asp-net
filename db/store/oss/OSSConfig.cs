﻿using up6.utils;

namespace up6.db.store.oss
{
    public class OSSConfig
    {
        public string ak = "minioadmin";
        public string sk = "minioadmin";
        public string endpoint = "192.168.0.111:9000";
        public string bucket = "test";

        public OSSConfig()
        {
            ConfigReader cr = new ConfigReader();
            this.ak = cr.readString("$.OSS.ak");
            this.sk = cr.readString("$.OSS.sk");
            this.endpoint = cr.readString("$.OSS.endpoint");
            this.bucket = cr.readString("$.OSS.bucket");
        }
    }
}