﻿using System.IO;
using up6.db.model;
using up6.db.utils;
using up6.utils;

namespace up6.db.store.oss
{
    public class OSSWriter : FileBlockWriter
    {
        public OSSWriter()
        {
            this.storage = StorageType.OSS;
        }

        /**
         * 生成UploadId
         */
        public override string make(FileInf file)
        {
            string ext = PathTool.getExtention(file.pathSvr);
            byte[] buf = new byte[0];
            string fileID = OSSTool.CreateMultipartUpload(file.ossKey());
            return fileID;
        }

        public override string write(FileInf file, Stream stm)
        {
            byte[] data = new byte[stm.Length];
            stm.Seek(0, SeekOrigin.Begin);
            stm.Read(data, 0, data.Length);
            stm.Close();
            stm = null;

            var etag = OSSTool.UploadPart(file.ossKey(), file.blockIndex, file.object_id, data);
            file.etag = etag;
            file.saveEtags();//保存
            return etag;
        }

        public override bool writeLastPart(FileInf file)
        {
            return OSSTool.CompleteMultipartUpload(file);
        }
    }
}