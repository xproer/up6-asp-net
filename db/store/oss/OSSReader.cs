﻿using up6.db.utils;

namespace up6.db.store.oss
{
    public class OSSReader : FileBlockReader
    {
        public override byte[] read(string fileName, long offset, long size)
        {
            fileName = PathTool.url_decode(fileName);
            return OSSTool.getObject(fileName, offset, size);
        }
    }
}