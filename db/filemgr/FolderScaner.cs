﻿using System.Collections.Generic;
using up6.db.database;
using up6.db.model;

namespace up6.db.filemgr
{
    public class FolderScaner : biz.FolderSchemaDB
    {
        /// <summary>
        /// 是否覆盖同路径文件？
        /// </summary>
        public bool m_cover = false;

        public override void save(FileInf dir)
        {
            this.m_root = dir;
            this.m_files = new List<FileInf>();
            this.m_folders = new List<FileInf>();
            this.m_dirs = new Dictionary<string, FileInf>();

            //加载文件信息
            this.loadFiles(dir);

            //分析
            this.parseParent();
            this.parseDirs();
            this.updatePID();
            this.updatePathRel();

            //删除根目录，根目录已经添加到数据表
            this.m_folders.RemoveAt(0);
            DBConfig cfg = new DBConfig();

            //覆盖同路径文件=>/dir1/dir2/name.txt
            if(this.m_cover) this.cover();

            //添加目录数据
            var db = cfg.folder();
            db.addBatch(this.m_folders);

            //添加文件数据
            var dbf = cfg.file();
            dbf.addBatch(this.m_files);
        }

    }
}