﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using up6.db.model;
using up6.db.utils;
using up6.filemgr.app;

namespace up6.db.biz
{
    public class FileEtag
    {

		/**
		 * 
		 * @param f
		 */
		public void saveTags(FileInf f)
		{
			String file = f.ETagsFile();
			PathTool.mkdirsFromFile(file);
			//System.out.println("ETag.file="+file);

			String val = JsonConvert.SerializeObject(f);//取消格式化
			FileTool.appendLine(file, val);
		}
	}
}