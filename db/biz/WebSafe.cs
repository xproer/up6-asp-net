﻿using up6.db.model;
using up6.db.utils;
using up6.filemgr.app;
using up6.utils;

namespace up6.db.biz
{
    /// <summary>
    /// Web安全模板
    /// </summary>
    public class WebSafe
    {
        /// <summary>
        /// 难证token
        /// </summary>
        /// <param name="token"></param>
        /// <param name="f"></param>
        /// <param name="action">动作：init,block</param>
        /// <returns></returns>
        public bool validToken(string token, FileInf f,string action="init")
        {
            if (ConfigReader.securityToken())
            {
                if (string.IsNullOrEmpty(token.Trim())) return false;
                CryptoTool ct = new CryptoTool();
                return ct.token(f,action) == token;
            }
            return true;
        }

        public bool downToken(string token,string id,string pathSvr,int blockIndex,long blockOffset) {
            if(string.IsNullOrEmpty(token)) return false;
            string[] arr = {
                id,
                //pathSvr,
                blockIndex.ToString(),
                blockOffset.ToString()
            };
            CryptoTool ct = new CryptoTool();
            var tokenSvr= ct.aes_cbc_pkcs(Md5Tool.calc(string.Join(",", arr)),false);
            return token == tokenSvr;
        }
    }
}