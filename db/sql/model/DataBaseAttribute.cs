﻿using System;
using System.Data;

namespace up6.sql.model
{
    [AttributeUsage(AttributeTargets.Class |
    AttributeTargets.Constructor |
    AttributeTargets.Field |
    AttributeTargets.Method |
    AttributeTargets.Property,
    AllowMultiple = true)]
    public sealed class DataBaseAttribute: Attribute
    {
        /// <summary>
        /// 数据表字段名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 数据表类型
        /// </summary>
        public DbType type { get; set; }
        public DataBaseAttribute(string name) {
            this.name = name;
        }
    }
}