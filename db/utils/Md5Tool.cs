﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace up6.db.utils
{
    public class Md5Tool
    {
        public static string calc(byte[] data)
        {
            MD5 md5 = MD5.Create();
            byte[] result = md5.ComputeHash(data);
            StringBuilder strbul = new StringBuilder(40);
            for (int i = 0; i < result.Length; i++)
            {
                strbul.Append(result[i].ToString("x2"));//加密结果"x2"结果为32位,"x3"结果为48位,"x4"结果为64位
            }
            return strbul.ToString();
        }

        public static string calc(string source)
        {
            byte[] sor = Encoding.UTF8.GetBytes(source);
            MD5 md5 = MD5.Create();
            byte[] result = md5.ComputeHash(sor);
            StringBuilder strbul = new StringBuilder(40);
            for (int i = 0; i < result.Length; i++)
            {
                strbul.Append(result[i].ToString("x2"));//加密结果"x2"结果为32位,"x3"结果为48位,"x4"结果为64位

            }
            return strbul.ToString();
        }

        public static string calc(Stream s)
        {
            byte[] data = new byte[s.Length];
            s.Read(data, 0, (int)s.Length);

            MD5 md5 = MD5.Create();
            byte[] result = md5.ComputeHash(data);
            StringBuilder strbul = new StringBuilder(40);
            for (int i = 0; i < result.Length; i++)
            {
                strbul.Append(result[i].ToString("x2"));//加密结果"x2"结果为32位,"x3"结果为48位,"x4"结果为64位
            }
            return strbul.ToString();
        }

        public static string calcShort(string v)
        {
            var id = calc(v);
            return id.Substring(0, 6);
        }

        public static string base16(string b) { 
            var buf = System.Text.Encoding.UTF8.GetBytes(b);
            var hash = new System.Text.StringBuilder();
            foreach (byte v in buf)
            {
                hash.Append(v.ToString("x2"));
            }
            return hash.ToString();
        }

        public static string base16(byte[] buf) {
            var hash = new System.Text.StringBuilder();
            foreach (byte v in buf)
            {
                hash.Append(v.ToString("x2"));
            }
            return hash.ToString();
        }

        /// <summary>
        /// SHA-256
        /// </summary>
        /// <param name="randomString"></param>
        /// <returns></returns>
        public static string sha256(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));

            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
        public static byte[] sha256(byte[] data)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(data);
            return crypto;
        }
    }
}