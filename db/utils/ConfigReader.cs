﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web;
using up6.db.store;
using up6.db.store.obs;
using up6.db.store.oss;
using up6.db.utils;

namespace up6.utils
{
    public enum StorageType
    {
        IO,FastDFS,Minio,OSS,OBS
    }
    public enum DataBaseType
    {
        SqlServer,Oracle,MySQL,MongoDB,ODBC,Kingbase
    }

    /// <summary>
    /// 系统配置文件读取
    /// ConfigReader cr = new ConfigReader();
    ///   选配置节点
    ///   cr.m_files.SelectToken("$.FastDFS")
    /// 
    ///   cr.module("名称");//获取json对象
    ///   cr.readFile("节点名称");//获取配置文件数据
    /// </summary>
    public class ConfigReader
    {
        public JToken m_files;

        public static ConfigReader build()
        {
            return new ConfigReader();
        }

        /// <summary>
        /// 自动加载/data/config/config.json配置文件
        /// </summary>
        public ConfigReader()
        {
            string file = HttpRuntime.AppDomainAppPath + "/config/config.json";
            this.m_files = JToken.Parse(File.ReadAllText(file));
        }

        public static DataBaseType dbType() {
            var cr = new ConfigReader();
            var tp = cr.readString("database.connection.type");
            if (string.Compare(tp, "oracle", true) == 0) return DataBaseType.Oracle;
            else if (string.Compare(tp, "kingbase", true) == 0) return DataBaseType.Kingbase;
            else if (string.Compare(tp, "mongodb", true) == 0) return DataBaseType.MongoDB;
            return DataBaseType.SqlServer;
        }

        /// <summary>
        /// 扩展名称在白名单中
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool inFileExts(string fileName) {
            var cr = ConfigReader.build();
            var exts = cr.readString("IO.exts");
            //未配置白名单
            if (string.IsNullOrEmpty(exts)) return true;

            var arr = exts.Split(',');
            var ext = PathTool.getExtention(fileName);
            for (int i = 0, l = arr.Length; i < l; ++i)
            {
                if (0 == string.Compare(ext, arr[i], true))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 存储类型:磁盘存储(IO),分布式存储(FastDFS)
        /// </summary>
        /// <returns></returns>
        public static StorageType storage()
        {
            var cr = new ConfigReader();
            var sto = cr.m_files.SelectToken("$.Storage.type").ToString().Trim().ToLower();
            if( string.Compare( sto,"fastdfs",true)==0) return StorageType.FastDFS;
            else if(string.Compare(sto,"minio",true)==0) return StorageType.Minio;
            else if(string.Compare(sto, "oss", true)==0) return StorageType.OSS;
            else if(string.Compare(sto, "obs", true)==0) return StorageType.OBS;
            return StorageType.IO;
        }

        /// <summary>
        /// 传输加密
        /// 配置：/config.json/security.encrypt
        /// </summary>
        /// <returns></returns>

        public static bool securityToken()
        {
            ConfigReader cr = new ConfigReader();
            var sec = cr.m_files;
            var encrypt = (bool)cr.m_files.SelectToken("$.security.token");
            return encrypt;
        }

        /// <summary>
        /// 存储加密，需要在前端开启传输加密
        /// /up6.js/security.encrypt
        /// </summary>
        /// <returns></returns>
        public static bool storageEncrypt()
        {
            var cr = new ConfigReader();
            var e =  bool.Parse( cr.m_files.SelectToken("$.Storage.encrypt").ToString().Trim());
            return e;
        }

        public static FileBlockWriter blockWriter() {
            var sto = storage();
            if (sto == StorageType.FastDFS) return new FastDFSWriter();
            else if (sto == StorageType.Minio) return new MinioWriter();
            else if (sto == StorageType.OSS) return new OSSWriter();
            else if (sto == StorageType.OBS) return new OBSWriter();
            else return new FileBlockWriter();
        }

        public static FileBlockReader blockReader()
        {
            if(storage()== StorageType.FastDFS) return new FastDFSReader();
            else if(storage()== StorageType.Minio) return new MinioReader();
            else if(storage()== StorageType.OSS) return new OSSReader();
            else if(storage()== StorageType.OBS) return new OBSReader();
            else return new FileBlockReader();
        }

        /// <summary>
        /// 自动加载网站中的文件
        /// </summary>
        /// <param name="f">/data/file.txt</param>
        /// <returns></returns>
        public string loclFile(string f)
        {
            f = HttpRuntime.AppDomainAppPath + f;
            return File.ReadAllText(f);
        }

        /// <summary>
        /// 自动加载模块
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public JToken module(string name)
        {
            string file = (string)this.m_files.SelectToken(name);
            file = HttpRuntime.AppDomainAppPath + file;
            var o = JToken.Parse( File.ReadAllText(file ));
            return o;
        }

        public string readFile(string name)
        {
            string file = (string)this.m_files.SelectToken(name);
            file = HttpRuntime.AppDomainAppPath + file;
            return File.ReadAllText(file);
        }

        public JToken readJson(string name)
        {
            string file = (string)this.m_files.SelectToken(name);
            file = HttpRuntime.AppDomainAppPath + file;
            var o = JToken.Parse(File.ReadAllText(file));
            return o;
        }

        /// <summary>
        /// 读取config.json中的值
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string readString(string name)
        {
            string v = (string)this.m_files.SelectToken(name);
            return v;
        }

        /// <summary>
        /// 读取config.json中的int值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public int readInt(string key)
        {
            return int.Parse((string)this.m_files.SelectToken(key));
        }

        public bool readBool(string name)
        {
            var v = (bool)this.m_files.SelectToken(name);
            return v;
        }

        /// <summary>
        /// 读取/data/config/目录下的文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public Dictionary<string,object> read(string fileName)
        {
            string ps = HttpRuntime.AppDomainAppPath + "/data/config/"+fileName;
            string data = File.ReadAllText(ps);
            var m = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            return m;
        }

        /// <summary>
        /// 读取/data/config目录下的文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public JToken configFile(string fileName)
        {
            string ps = HttpRuntime.AppDomainAppPath + "/data/config/" + fileName;
            string data = File.ReadAllText(ps);
            var m = JToken.Parse(data);
            return m;
        }
    }
}