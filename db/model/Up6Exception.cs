﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace up6.db.model
{
    public class Up6Exception : ApplicationException
    {
        public Up6Exception(string msg):base(msg) { }
        public Up6Exception(string msg,FileInf f) : base(msg) {
            string json = JsonConvert.SerializeObject(f);//取消格式化
            System.Diagnostics.Debug.WriteLine(json);
        }
    }
}