﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace up6.db.model
{
    public class Message
    {
        public Message(string msg,JObject o)
        {
            System.Diagnostics.Debug.WriteLine(msg);
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(o));
        }
        public Message(JObject o)
        {
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(o));
        }

        public Message(string msg)
        {
            System.Diagnostics.Debug.WriteLine(msg);
        }
        public Message(FileInf f)
        {
            string json = JsonConvert.SerializeObject(f);//取消格式化
            System.Diagnostics.Debug.WriteLine(json);
        }

        public Message(string msg,FileInf f)
        {
            System.Diagnostics.Debug.WriteLine(msg);
            string json = JsonConvert.SerializeObject(f);//取消格式化
            System.Diagnostics.Debug.WriteLine(json);
        }

        public Message msg(string msg) {
            System.Diagnostics.Debug.WriteLine(msg);
            return this;
        }
        public Message msg(string m,string v)
        {
            System.Diagnostics.Debug.WriteLine(m);
            System.Diagnostics.Debug.WriteLine(v);
            return this;
        }

        public Message msg(Exception e)
        {
            System.Diagnostics.Debug.WriteLine(e.ToString());
            return this;
        }
    }
}