﻿using System.Data;
using up6.sql.model;

namespace up6.db.model
{
    public class DnFileInf : FileInf
    {
        public DnFileInf() 
        {
            this.fdTask = false;
        }
        //与up6_files..f_id关联
        public string f_id= string.Empty;
        /// <summary>
        /// MAC地址
        /// </summary>
        public string mac = string.Empty;
        /// 服务器文件路径
        /// </summary>
        [DataBase("f_fileUrl")]
        public string fileUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        [DataBase("f_sizeSvr")]
        public string sizeSvr = "0byte";
        /// <summary>
        /// 传输进度
        /// </summary>
        [DataBase("f_perLoc")]
        public string perLoc = "0%";
        /// <summary>
        /// 远程文件加密大小
        /// </summary>
        public long lenSvrSec = 0;
    }
}