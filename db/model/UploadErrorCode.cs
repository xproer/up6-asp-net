﻿namespace up6.db.model
{
    public class UploadErrorCode
    {
        /// <summary>
        /// 加密块大小不相同，服务器接收的块大小与控件上传的块大小不相同
        /// </summary>
        public static string blockSizeCryDifferent = "blockSizeCryDifferent";
        public static string blockSizeCprDifferent = "blockSizeCprDifferent";
        /// <summary>
        /// 块大小不相同，服务器接收的块大小与控件上传的块大小不相同
        /// </summary>
        public static string blockSizeDifferent = "blockSizeDifferent";
        /// <summary>
        /// 块MD5不匹配，服务器接收的块MD5和计算的块MD5不匹配
        /// </summary>
        public static string blockMd5Different = "blockMd5Different";
        public static string blockDataEmpty = "blockDataEmpty";
        /// <summary>
        /// token安全验证失败
        /// </summary>
        public static string tokenVerFail= "tokenVerFail";
        /// <summary>
        /// 写入块数据错误
        /// </summary>
        public static string writeBlockDataFail = "writeBlockDataFail";

        public UploadErrorCode() { }
    }
}