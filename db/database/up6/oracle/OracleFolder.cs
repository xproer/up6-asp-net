﻿using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using up6.db.model;
using up6.db.sql;
using up6.filemgr.app;
using up6.sql;

namespace up6.db.database.up6.oracle
{
    public class OracleFolder : sql.SqlFolder
    {
        public override void Remove(string id, string uid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("begin ");
            sb.Append("update up6_files set f_deleted=1 where f_id=:f_id and f_uid=:f_uid;");
            sb.Append("update up6_files set f_deleted=1 where f_pidRoot=:f_id and f_uid=:f_uid;");
            sb.Append("update up6_folders set f_deleted=1 where f_id=:f_id and f_uid=:f_uid;");
            sb.Append(" end;");

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sb.ToString());
            db.AddString(ref cmd, ":f_id", id, 32);
            db.AddString(ref cmd, ":f_uid", uid, 255);
            db.ExecuteNonQuery(cmd);
        }
    }
}