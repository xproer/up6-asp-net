﻿using MongoDB.Driver;
using up6.utils;

namespace up6.db.database.up6.mongo
{
    public class MongoConfig
    {
        public MongoConfig() { }
        public static MongoClient client() {
            ConfigReader c = new ConfigReader();
            var str = c.readString("database.connection.mongodb.addr");
            return new MongoClient(str);
        }
    }
}