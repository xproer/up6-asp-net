﻿using MongoDB.Driver;
using System.Collections.Generic;
using up6.db.model;

namespace up6.db.database.up6.mongo
{
    public class MongoFolder: sql.SqlFolder
    {
        /// <summary>
        /// 数据表：up6_files
        /// </summary>
        /// <returns></returns>
        public IMongoCollection<FileInf> table()
        {
            var db = MongoConfig.client().GetDatabase("up6");
            return db.GetCollection<FileInf>("up6_folders");
        }

        public override FileInf read(string id) {
            MongoFile mf = new MongoFile();
            return mf.read(id);
        }

        public override void Remove(string id, string uid)
        {
            //up6_folders
            this.table().UpdateOne(Builders<FileInf>.Filter.Eq(f => f.id, id),
                Builders<FileInf>.Update.Set(f => f.deleted, true)
                );

            //up6_files
            MongoFile files = new MongoFile();
            files.Delete(uid, id);

            //up6_files childs
            files.table().UpdateMany(Builders<FileInf>.Filter.Eq(f => f.pidRoot, id),
                Builders<FileInf>.Update.Set(f=>f.deleted,true));
        }

        public override void del(string id, string uid)
        {
            this.table().UpdateOne(Builders<FileInf>.Filter.Eq(f => f.id, id),
                Builders<FileInf>.Update.Set(f => f.deleted, true));
        }

        public override void Clear()
        {
            this.table().DeleteMany(Builders<FileInf>.Filter.Empty);
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="arr"></param>
        public override void addBatch(List<FileInf> arr)
        {
            if(arr.Count>0) this.table().InsertMany(arr);
        }
    }
}