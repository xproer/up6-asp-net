﻿using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using up6.db.database.up6.mongo;
using up6.db.model;
using up6.db.sql;
using up6.sql;
using up6.utils;

namespace up6.db.database.up6.sql
{
    public class SqlFolder
    {
        public static SqlFolder build()
        {
            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.Oracle) return new up6.oracle.OracleFolder();
            if (tp == DataBaseType.Kingbase || tp == DataBaseType.ODBC) return new up6.odbc.OdbcFolder();
            if (tp == DataBaseType.MongoDB) return new MongoFolder();
            else return new up6.sql.SqlFolder();
        }

        public virtual FileInf read(string id)
        {
            var t = new SqlTable("up6_folders");
            return t.readOne<FileInf>(SqlWhere.build().eq("f_id", id));
        }

        public virtual FileInf read(string pathRel, string id, string uid)
        {
            return SqlTable.build("up6_folders").readOne<FileInf>("f_id,f_pid,f_pidRoot,f_pathSvr,f_pathRel",
                SqlWhere.build()
                .eq("f_pathRel", pathRel)
                .eq("f_deleted", false)
                .eq("f_uid", uid)
                .ineq("f_id", id)
                );
        }

        public virtual void Remove(string id, string uid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update up6_files set f_deleted=1 where f_id=@id and f_uid=@uid;");
            sb.Append("update up6_files set f_deleted=1 where f_pidRoot=@id and f_uid=@uid;");
            sb.Append("update up6_folders set f_deleted=1 where f_id=@id and f_uid=@uid;");

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sb.ToString());
            db.AddString(ref cmd, "@id", id,32);
            db.AddString(ref cmd, "@uid", uid,255);
            db.ExecuteNonQuery(cmd);
        }

        public virtual void del(string id,string uid)
        {
            SqlTable.build("up6_files").update(
                SqlSeter.build().set("f_deleted", true), 
                SqlWhere.build()
                .eq("f_id",id)
                .eq("f_uid",uid));

            SqlTable.build("up6_folders").update(
                SqlSeter.build().set("f_deleted", true),
                SqlWhere.build()
                .eq("f_id", id)
                .eq("f_uid", uid));
        }

        public virtual void Clear()
        {
            SqlTable.build("up6_folders").clear();
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="arr"></param>
        public virtual void addBatch(List<FileInf> arr)
        {
            SqlTable.build("up6_folders").inserts<FileInf>(arr.ToArray());
        }

        public virtual void add(FileInf f)
        {
            SqlTable.build("up6_folders").insert(f);
        }

        public virtual void complete(string id,string uid)
        {
            SqlTable.build("up6_folders").update(
                SqlSeter.build().set("f_complete", true),
                SqlWhere.build().eq("f_id",id)
                .eq("f_uid",uid)
                );
        }
    }
}