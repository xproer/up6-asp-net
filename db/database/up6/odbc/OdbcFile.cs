﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using up6.db.model;
using up6.db.utils;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace up6.db.database.up6.odbc
{
    public class OdbcFile : sql.SqlFile
    {
        public override void complete(string id)
        {
            string sql = "update up6_files set f_lenSvr=f_lenLoc,f_perSvr='100%',f_complete=True,f_scan=True where f_id=?";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);

            db.AddString(ref cmd, "@f_id", id, 32);
            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 删除一条数据，并不真正删除，只更新删除标识。
        /// </summary>
        /// <param name="f_uid"></param>
        /// <param name="f_id"></param>
        public override void Delete(string f_uid, string f_id)
        {
            string sql = "update up6_files set f_deleted=True where f_uid=? and f_id=?";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);

            db.AddString(ref cmd, "@f_uid", f_uid, 255);
            db.AddString(ref cmd, "@f_id", f_id, 32);
            db.ExecuteNonQuery(cmd);
        }

        public override void delete(string pathRel, string uid, string id)
        {
            string sql = "update up6_files set f_deleted=True where f_pathRel=? and f_uid=? and f_id!=?";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);

            //if (string.IsNullOrEmpty(pid)) pid = " ";
            //db.AddString(ref cmd, "@pid", pid, 32);
            db.AddString(ref cmd, "@pathRel", pathRel, 255);
            db.AddString(ref cmd, "@f_uid", uid, 255);
            db.AddString(ref cmd, "@f_id", id, 32);
            db.ExecuteNonQuery(cmd);
        }
    }
}