﻿using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using up6.db.database.up6.mongo;
using up6.db.model;
using up6.db.utils;

namespace up6.db.database.down2.mongo
{
    public class MongoFile: down2.sql.SqlFile
    {
        /// <summary>
        /// 数据表：up6_files
        /// </summary>
        /// <returns></returns>
        private IMongoCollection<DnFileInf> table()
        {
            var db = MongoConfig.client().GetDatabase("up6");
            return db.GetCollection<DnFileInf>("down_files");
        }

        public override void Add(ref DnFileInf inf)
        {
            this.table().InsertOne(inf);
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fid"></param>
        public override void Delete(string fid, string uid)
        {
            this.table().DeleteOne(f => f.id == fid);
        }

        public override void Clear()
        {
            this.table().DeleteMany(Builders<DnFileInf>.Filter.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public override string all_uncmp(string uid)
        {
            var fs = this.table().Find(f=>f.uid==uid).ToList();
            return Newtonsoft.Json.JsonConvert.SerializeObject(fs);
        }

        /// <summary>
        /// 从up6_files表中加载所有已经上传完毕的文件
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public override string all_complete(string uid)
        {
            var fs = new database.up6.mongo.MongoFile();
            var cmps = fs.table().Find(f => f.complete == true 
                                            && f.uid == uid
                                            && f.deleted == false
                                            && f.fdChild == false
                                            && f.scaned == true).ToList();

            List<DnFileInf> ds = new List<DnFileInf>();
            foreach (var f in cmps)
            {
                var d = new DnFileInf();
                d.id = Guid.NewGuid().ToString("N");
                d.f_id = f.id;
                d.fdTask = f.fdTask;
                d.nameLoc = f.nameLoc;
                d.sizeLoc = f.sizeLoc;
                d.sizeSvr = f.sizeLoc;
                d.lenSvr = f.lenSvr;
                d.pathSvr = f.pathSvr;
                d.lenLocSec = f.lenLocSec;
                d.blockSize = f.blockSize;
                d.blockSizeSec = f.blockSizeSec;
                d.object_key = f.object_key;
                d.encrypt = f.encrypt;
                d.encryptAgo = f.encryptAgo;
                ds.Add(d);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(ds);
        }

        public override string childs(string pidRoot)
        {
            up6.mongo.MongoFile mf = new up6.mongo.MongoFile();
            var fs = mf.table().Find(f => f.pidRoot == pidRoot).ToList();
            List<JObject> js = new List<JObject>();
            foreach (var f in fs)
            {
                var o = JObject.FromObject(f);
                o["f_id"] = f.id;
                o["fields"] = new JObject { { "object_key", PathTool.url_safe_encode(f.object_key) } };
                js.Add(o);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(js);
        }
    }
}