﻿using Newtonsoft.Json;
using up6.db.database.down2.mongo;
using up6.db.model;
using up6.db.sql;
using up6.db.utils;
using up6.utils;

namespace up6.db.database.down2.sql
{
    public class SqlFile
    {
        public static SqlFile build()
        {
            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.MongoDB) return new MongoFile();
            return new SqlFile();
        }

        private SqlTable table() {
            return SqlTable.build("down_files");
        }

        public virtual void Add(ref DnFileInf inf)
        {
            this.table().insert(inf);
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fid"></param>
        public virtual void Delete(string fid, string uid)
        {
            this.table().delete(SqlWhere.build()
                .eq("f_id", fid)
                .eq("f_uid", uid));
        }

        public virtual void process(string fid, string uid, long lenLoc, string perLoc)
        {
            this.table().update(SqlSeter.build()
                .set("f_lenLoc", lenLoc)
                .set("f_perLoc", perLoc)
                , SqlWhere.build()
                .eq("f_id", fid)
                .eq("f_uid", uid));
        }

        public virtual void Clear()
        {
            this.table().clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public virtual string all_uncmp(string uid)
        {
            var files = this.table().reads<DnFileInf>(SqlWhere.build()
                .eq("f_uid", uid)
                .eq("f_complete",false));
            return JsonConvert.SerializeObject(files);
        }

        /// <summary>
        /// 从up6_files表中加载所有已经上传完毕的文件
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public virtual string all_complete(string uid)
        {
            var fs = SqlTable.build("up6_files").reads<DnFileInf>(SqlWhere.build()
                .eq("f_uid", uid)
                .eq("f_deleted", false)
                .eq("f_complete", true)
                .eq("f_fdChild", false)
                .eq("f_scan", true));

            foreach(var f in fs){
                f.f_id = f.id;
                f.sizeSvr = f.sizeLoc;
                f.lenSvrSec = f.lenLocSec;
                f.fields.Add("object_key", f.object_key);
            }
            return JsonConvert.SerializeObject(fs);
        }
        /// <summary>
        /// 取根目录下所有子文件
        /// 注意：传进来的必须是根目录ID
        /// </summary>
        /// <param name="pidRoot"></param>
        /// <returns></returns>
        public virtual string childs(string pidRoot)
        {
            var files = SqlTable.build("up6_files").reads<DnFileInf>(SqlWhere.build()
                .eq("f_pidRoot", pidRoot));

            foreach (var f in files)
            {
                f.f_id = f.id;
                f.lenSvrSec = f.lenLocSec;
                f.fields.Add("object_key",PathTool.url_safe_encode(f.object_key));
            }
            return JsonConvert.SerializeObject(files);
        }
    }
}