﻿using up6.db.biz;
using up6.db.biz.folder;
using up6.db.database.up6.mongo;
using up6.sql;
using up6.utils;

namespace up6.db.database
{
    public class DBConfig
    {
        public DBConfig()
        {
            DbHelper db = new DbHelper();
        }

        public up6.sql.SqlFile file()
        {
            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.MongoDB) return new MongoFile();
            else return new up6.sql.SqlFile();
        }

        public up6.sql.SqlFolder folder()
        {
            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.Oracle) return new up6.oracle.OracleFolder();
            if (tp == DataBaseType.MongoDB) return new MongoFolder();
            else return new up6.sql.SqlFolder();
        }

        public down2.sql.SqlFile downFile()
        {
            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.MongoDB) return new down2.mongo.MongoFile();
            else return new down2.sql.SqlFile();
        }

        public SqlExec se()
        {
            var tp = ConfigReader.dbType();
            var exec = new SqlExec();
            if (tp == DataBaseType.Oracle) exec = new OracleExec();
            if (tp == DataBaseType.Kingbase || tp == DataBaseType.ODBC) exec = new OdbcExec();
            return exec;
        }

        public DbBase bs()
        {
            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.Oracle) return new OracleDbBase();
            if (tp == DataBaseType.Kingbase || tp == DataBaseType.ODBC) return new OdbcDbBase();
            else return new DbBase();
        }

    }
}