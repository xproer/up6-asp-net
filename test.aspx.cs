﻿using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Asn1.Nist;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using up6.db.utils;
using up6.utils;

namespace up6
{
    public partial class test : WebBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            aes_ecb_encode();
            aes_ecb_decode();
            return;

            string ps = "/a/b/c/d/e/f/g";

            //解析后最后一个路径丢失，
            List<string> rels = new List<string>();
            int pos = 0;
            while ((pos = ps.IndexOf("/", pos)) != -1)
            {
                var dir = ps.Substring(0, pos);
                if (string.IsNullOrEmpty(dir)) dir = "/";
                rels.Add(dir);
                pos++;
            }

            this.toContent(JToken.FromObject(rels).ToString());
        }

        void sm4_decode() {
            CryptoTool ct = new CryptoTool();
            var s = ct.sm4_decode(File.ReadAllBytes("F:/asp.net/up6/upload/2023/05/09/af77dfa3151448e8909c52ab05c47bce/key.txt"));
            Response.Write(UTF8Encoding.UTF8.GetString(s.ToArray()) );
        }

        void aes_ecb_decode() {
            CryptoTool c = new CryptoTool();
            string fin = "f:/ftp/b.txt";
            string fout = "f:/ftp/c.tsxt";
            byte[] bs = File.ReadAllBytes(fin);
            var s = c.aes_ecb_decode(new MemoryStream(bs));

            File.WriteAllBytes(fout,s.ToArray());
        }

        void aes_ecb_encode()
        {
            CryptoTool c = new CryptoTool();
            string fin = "f:/ftp/a.txt";
            string fout = "f:/ftp/b.txt";
            byte[] bs = File.ReadAllBytes(fin);
            var s = c.aes_ecb_encode(new MemoryStream(bs));

            File.WriteAllBytes(fout, s.ToArray());
        }

        void sm4_ecb_encode()
        {
            CryptoTool c = new CryptoTool();
            string s = "123456";
            Response.Write(s);
            Response.Write("<br/>");

            s = c.sm4_ecb_encode(s);
            Response.Write("加密结果：");
            Response.Write(s);
            Response.Write("<br/>");
        }

        void sm4_cbc_encode()
        {
            CryptoTool c = new CryptoTool();
            string s = "123456";
            Response.Write(s);
            Response.Write("<br/>");

            s = c.sm4_encode(s);
            Response.Write("加密结果：");
            Response.Write(s);
            Response.Write("<br/>");

            s = c.sm4_decode(s);
            Response.Write("解密结果：");
            Response.Write(s);
            Response.Write("<br/>");
        }

        void aes_cbc_encode() {
            CryptoTool c = new CryptoTool();
            string s = "123456";
            Response.Write(s);
            Response.Write("<br/>");

            s = c.aes_cbc_pkcs(s);
            Response.Write("加密结果：");
            Response.Write(s);
            Response.Write("<br/>");

            s = c.aes_decode(s);
            Response.Write("解密结果：");
            Response.Write(s);
            Response.Write("<br/>");
        }

        void sm4_file_decode() {
            string path = "F:/ftp/test.exe";
            string dec = "F:/ftp/test-dec.exe";

            var bytes = File.ReadAllBytes(path);
            CryptoTool ct = new CryptoTool();
            byte[] buf = new byte[2097168];
            MemoryStream ms1 = new MemoryStream(buf);
            ms1.Write(bytes,0, buf.Length);
            //bytes.CopyTo(buf, 0);
            var o = ct.sm4_decode(buf);

            MemoryStream msSrc = new MemoryStream();
            var data = o.ToArray();
            msSrc.Write(data, 0, data.Length);

            //buf = new byte[bytes.Length - 2097168];
            ms1.SetLength(bytes.Length - 2097168);
            //ms1 = new MemoryStream(buf);
            ms1.Write(bytes, 2097168, (int)ms1.Length);
            //bytes.CopyTo(buf, 2097168);
            o = ct.sm4_decode(ms1.ToArray());
            data = o.ToArray();
            msSrc.Write(data,0, data.Length);
            
            File.WriteAllBytes(dec, msSrc.ToArray());
        }
	}
}