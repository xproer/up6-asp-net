function Down2App(mgr)
{
    var _this = this;
    this.ins = mgr;
    this.edgeApp = mgr.edgeApp;
    this.Config = mgr.Config;
    this.queueCount = 0;
    this.checkFF = function () {
        var mimetype = navigator.mimeTypes;
        if (typeof mimetype == "object" && mimetype.length) {
            for (var i = 0; i < mimetype.length; i++) {
                var enabled = mimetype[i].type == this.Config.firefox.type;
                if (!enabled) enabled = mimetype[i].type == this.Config.firefox.type.toLowerCase();
                if (enabled) return mimetype[i].enabledPlugin;
            }
        }
        else {
            mimetype = [this.Config.firefox.type];
        }
        if (mimetype) {
            return mimetype.enabledPlugin;
        }
        return false;
    };
    this.Setup = function () {
        //文件夹选择控件
        acx += '<object id="downPart" classid="clsid:' + this.Config.ClsidPart + '"';
        acx += ' codebase="' + this.Config.CabPath + '" width="1" height="1" ></object>';

        $("body").append(acx);
    };
    this.init = function () {
        var param = { name: "init", config: this.Config };
        this.postMessage(param);
    };
    this.initNat = function () {
        if (!this.chrome45) return;
        this.exitEvent();
        document.addEventListener('Down3EventCallBack', function (evt) {
            this.recvMessage(JSON.stringify(evt.detail));
        });
    };
    this.initEdge = function () {
        this.edgeApp.run();
    };
    this.exit = function () {
        var par = { name: 'exit' };
        this.postMessage(par);
    };
    this.exitEvent = function () {
        var obj = this;
        $(window).bind("beforeunload", function () { obj.exit(); });
    };
    this.openFolder = function () {
        var param = { name: "open_folder" };
        this.postMessage(param);
    };
    this.openPath = function (f) {
        var param = $.extend({}, f, { name: "open_path" });
        this.postMessage(param);
    };
    this.openFile = function (f) {
        var param = $.extend(param, f, { name: "open_file" });
        this.postMessage(param);
    };
    this.openChild = function (f) {
        var param = $.extend(param, f, { name: "open_child" });
        this.postMessage(param);
    };
    this.addUrl = function (f) {
        this.queueCount++;
        var param = $.extend(param, f, { name: "add_url" });
        this.postMessage(param);
    };
    this.addUrls = function (f) {
        this.queueCount++;
        var param = $.extend(param, { name: "add_urls", urls: f });
        this.postMessage(param);
    };
    this.addJson = function (f) {
        this.queueCount++;
        var param = { name: "add_json" };
        $.extend(param, f);
        this.postMessage(param);
    };
    this.downFile = function (f) {
        this.queueCount++;
        var param = $.extend({}, f, { name: "down_file" });
        this.postMessage(param);
    };
    this.downFolder = function (f) {
        this.queueCount++;
        var param = $.extend({}, f, { name: "down_folder" });
        this.postMessage(param);
    };
    this.downUrl = function (f) {
        this.queueCount++;
        var param = $.extend({}, f, { name: "down_url" });
        this.postMessage(param);
    };
    this.downUrls = function (f) {
        this.queueCount++;
        var param = $.extend({}, f, { name: "down_urls" });
        this.postMessage(param);
    };
    this.downJson = function (f) {
        this.queueCount++;
        var param = $.extend({}, f, { name: "down_json" });
        this.postMessage(param);
    };
    this.initFile = function (f) {
        this.queueCount++;
        var param = $.extend({}, f, { name: "init_file" });
        this.postMessage(param);
    };
    this.initFolder = function (f) {
        this.queueCount++;
        var param = $.extend({}, f, { name: "init_folder" });
        this.postMessage(param);
    };
    this.initFolderJson = function (f) {
        this.queueCount++;
        //{files_json:"服务器返回的目录字符串（未进行url解码）"}
        var param = $.extend({}, f, { name: "init_folder_json" });
        this.postMessage(param);
    };
    this.addFile = function (f) {
        this.queueCount++;
        var param = $.extend({}, f, { name: "add_file" });
        this.postMessage(param);
    };
    this.addFolder = function (f) {
        this.queueCount++;
        var param = { name: "add_folder" };
        $.extend(param, f, { name: "add_folder" });
        this.postMessage(param);
    };
    this.stopFile = function (f) {
        this.queueCount--;
        var param = { name: "stop_file", id: f.id };
        this.postMessage(param);
    };
    this.delFile = function (f) {
        this.queueCount--;
        var param = { name: "del_file", id: f.id };
        this.postMessage(param);
    };
    this.postMessage = function (json) {
        try {
            this.ins.parter.postMessage(JSON.stringify(json));
        } catch (e) { console.log("调用postMessage失败，请检查控件是否安装成功"); }
    };
    this.postMessageNat = function (par) {
        var evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(this.entID, true, false, par);
        document.dispatchEvent(evt);
    };
    this.postMessageEdge = function (par) {
        this.edgeApp.send(par);
    };
}