﻿using System;
using System.Web;
using up6.db.database;
using up6.db.database.down2.sql;
using up6.utils;

namespace api.down2
{
    /// <summary>
    /// 列出未完成的文件和文件夹下载任务。
    /// 格式：json
    ///     [f1,f2,f3,f4]
    /// f1为xdb_files对象
    /// </summary>
    public partial class f_list : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string uid = this.reqString("uid");
            string cbk = this.reqString("callback");

            var json = HttpUtility.UrlEncode(SqlFile.build().all_uncmp(uid));
            json = json.Replace("+", "%20");
            json = cbk + "({\"value\":\"" + json + "\"})";

            this.toContentJson(json);
        }
    }
}