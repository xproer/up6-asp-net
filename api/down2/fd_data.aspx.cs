﻿using System;
using System.Web;
using up6.db.database.down2.sql;
using up6.utils;

namespace api.down2
{
    /// <summary>
    /// 获取文件夹JSON数据
    /// </summary>
    public partial class fd_data : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id  = this.reqString("id");
            string cbk = this.reqString("callback");
            string json = "({\"value\":null})";

            if (!string.IsNullOrEmpty(id))
            {
                string data = HttpUtility.UrlEncode(SqlFile.build().childs(id));
                data = data.Replace("+", "%20");

                json = "({\"value\":\""+ data + "\"})" ;
            }
            this.toContent(cbk + json, "application/json");
        }
    }
}