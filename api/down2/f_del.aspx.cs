﻿using System;
using up6.db.database;
using up6.filemgr.app;
using up6.utils;

namespace api.down2
{
    public partial class f_del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fid = Request.QueryString["id"];
            string uid = Request.QueryString["uid"];
            string cbk = Request.QueryString["callback"];

            if (string.IsNullOrEmpty(uid)
                || string.IsNullOrEmpty(fid))
            {
                this.toContentJson(cbk + "({\"value\":null})");
                return;
            }

            DBConfig cfg = new DBConfig();
            global::up6.db.database.down2.sql.SqlFile db = cfg.downFile();
            db.Delete(fid, uid);

            this.toContentJson(cbk + "({\"value\":1})");
        }
    }
}