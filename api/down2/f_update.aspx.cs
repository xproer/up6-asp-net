﻿using System;
using up6.db.database;
using up6.utils;

namespace api.down2
{
    /// <summary>
    /// 更新文件下载进度
    /// </summary>
    public partial class f_update : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fid      = this.reqString("id");
            string uid      = this.reqString("uid");
            var lenLoc   = this.reqLong("lenLoc");
            string per      = this.reqString("perLoc");
            string cbk      = this.reqString("callback");
            //

            if (    string.IsNullOrEmpty(uid)
                ||  string.IsNullOrEmpty(fid)
                ||  string.IsNullOrEmpty(cbk))
            {
                this.toContent(cbk + "({\"value\":0})", "application/json");
                return;
            }

            var db = global::up6.db.database.down2.sql.SqlFile.build();
            db.process( fid, uid, lenLoc, per);
            
            this.toContent(cbk + "({\"value\":1})", "application/json");
        }
    }
}