﻿using System;
using up6.db.database;

namespace api.down2
{
    public partial class clear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DBConfig cfg = new DBConfig();
            cfg.downFile().Clear();
        }
    }
}