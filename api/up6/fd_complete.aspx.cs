﻿using System;
using up6.db.biz;
using up6.db.database.up6.sql;
using up6.db.model;
using up6.utils;

namespace api.up6
{
    /// <summary>
    /// 更新记录：
    ///   2022-12-11
    ///     取消单独设置子文件和目录状态操作
    ///   2022-12-10 
    ///     完善逻辑，将上传完毕的状态更新放在最后面，防止子文件数据未插入完用户就开始下载
    /// </summary>
    public partial class fd_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqString("id");
            var uid = this.reqString("uid");
            string cak = this.reqString("callback");
            int ret = 0;

            if ( string.IsNullOrEmpty(id) )
            {
            }
            else
            {
                var db = SqlFile.build();
                FileInf folder = db.read(id);
                folder.uid = uid;

                //根节点
                FileInf root = new FileInf();
                root.id = folder.pidRoot;
                root.uid = folder.uid;
                //当前节点是根节点
                if (string.IsNullOrEmpty(root.id)) root.id = folder.id;

                //保存层级结构-解析层级信息文件
                FolderSchemaDB fsd = new FolderSchemaDB();
                fsd.save(folder);

                //上传完毕
                db.complete(id);

                up6_biz_event.folder_post_complete(id);

                ret = 1;
            }
            this.toContentJson(cak + "(" + ret + ")");
        }
    }
}