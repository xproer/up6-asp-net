﻿using System;
using up6.db.biz;
using up6.db.database;
using up6.db.database.up6.sql;
using up6.utils;

namespace api.up6
{
    /// <summary>
    /// 更新文件或文件夹进度，百分比，
    /// </summary>
    public partial class f_process : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id       = this.reqString("id");
            string uid      = this.reqString("uid");
            long offset   = this.reqLong("offset");
            long lenSvr   = this.reqLong("lenSvr");
            string perSvr   = this.reqString("perSvr");
            string callback = this.reqString("callback");//jsonp参数

            string json = callback + "({\"state\":0})";//返回jsonp格式数据。
            if(    !string.IsNullOrEmpty(id)&&
                !string.IsNullOrEmpty(perSvr))
            {
                SqlFile.build().process(uid,id,offset, lenSvr, perSvr);
                
                up6_biz_event.file_post_process(id);
                json = callback + "({\"state\":1})";//返回jsonp格式数据。
            }
            this.toContentJson(json);
        }
    }
}