﻿using System;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using up6.db.biz;
using up6.db.database.up6.sql;
using up6.db.model;
using up6.db.utils;
using up6.utils;

namespace api.up6
{
    /// <summary>
    /// 以guid模式存储文件夹，
    /// </summary>
    public partial class fd_create : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id       = this.reqString("id");
            string pid      = this.reqString("pid");
            string pidRoot  = this.reqString("pidRoot");
            string uid      = this.reqString("uid");
            string lenLoc   = this.reqString("lenLoc");
            string sizeLoc  = this.reqString("sizeLoc");
            string pathLoc  = this.reqStringDecode("pathLoc");
            string callback = this.reqString("callback");//jsonp参数
            if (string.IsNullOrEmpty(pid)) pid = string.Empty;
            if (string.IsNullOrEmpty(pidRoot)) pidRoot = pid;

            if (string.IsNullOrEmpty(id)
                || string.IsNullOrEmpty(uid)
                || string.IsNullOrEmpty(pathLoc)
                )
            {
                Response.Write(callback + "({\"value\":null})");
                return;
            }

            FileInf fileSvr = new FileInf();
            fileSvr.id      = id;
            fileSvr.pid     = pid;
            fileSvr.pidRoot = pidRoot;
            fileSvr.fdChild = false;
            fileSvr.fdTask  = true;
            fileSvr.uid     = uid;//将当前文件UID设置为当前用户UID
            fileSvr.nameLoc = Path.GetFileName(pathLoc);
            fileSvr.pathLoc = pathLoc;
            fileSvr.pathRel = "";
            fileSvr.lenLoc  = Convert.ToInt64(lenLoc);
            fileSvr.sizeLoc = sizeLoc;
            fileSvr.deleted = false;
            fileSvr.nameSvr = fileSvr.nameLoc;

            //生成存储路径
            PathBuilderUuid pb = new PathBuilderUuid();
            fileSvr.pathSvr    = pb.genFolder(ref fileSvr);
            fileSvr.pathSvr    = fileSvr.pathSvr.Replace("\\", "/");
            if(!PathTool.mkdir(fileSvr.pathSvr))
            {
                this.toContent("create dir error", 500);
                return;
            }

            //创建层级信息文件
            FolderSchema fs = new FolderSchema();
            if(!fs.create(fileSvr))
            {
                this.toContent("create schema file error", 500);
                return;
            }
            SqlFile.build().Add(ref fileSvr);

            up6_biz_event.folder_create(fileSvr);

            string json = JsonConvert.SerializeObject(fileSvr);
            json = HttpUtility.UrlEncode(json);
            json = json.Replace("+", "%20");
            var jo = new JObject { { "value",json} };
            json = callback + string.Format("({0})",JsonConvert.SerializeObject(jo));
            this.toContentJson(json);
        }
    }
}