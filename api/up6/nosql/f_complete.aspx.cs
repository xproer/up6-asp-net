﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using up6.db.biz;
using up6.db.database;
using up6.db.model;
using up6.utils;

namespace up6.api.up6.nosql
{
    public partial class f_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var md5 = this.reqString("md5");
            var uid = this.reqString("uid");
            var id = this.reqString("id");
            var pid = this.reqString("pid");
            var cbk = this.reqStringSafe("callback");
            var nameLoc = this.reqStringDecode("nameLoc");//文件名称

            //返回值。1表示成功
            int ret = 0;

            if (string.IsNullOrEmpty(id))
            {
            }//参数不为空
            else
            {
                up6_biz_event.file_post_complete(id);
                ret = 1;
            }
            this.toContentJson(cbk + "(" + ret + ")");//必须返回jsonp格式数据
        }
    }
}