﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using up6.db.database;
using up6.utils;

namespace up6.api.up6.nosql
{
    public partial class fd_del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fid = this.reqString("id");
            string uid = this.reqString("uid");
            string cbk = this.reqString("callback");
            int ret = 0;

            if (string.IsNullOrEmpty(fid) ||
                string.IsNullOrEmpty(uid)
                )
            {
            }//参数不为空
            else
            {
                ret = 1;
            }
            this.toContentJson(cbk + "({\"value\":" + ret + "})");//返回jsonp格式数据
        }
    }
}