﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using up6.db.biz;
using up6.db.database;
using up6.db.model;
using up6.utils;

namespace up6.api.up6.nosql
{
    public partial class fd_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqString("id");
            string uid = this.reqString("uid");
            string cak = this.reqString("callback");
            int ret = 0;

            if (string.IsNullOrEmpty(id) ||
                uid.Length < 1)
            {
            }
            else
            {
                up6_biz_event.folder_post_complete(id);

                ret = 1;
            }
            this.toContentJson(cak + "(" + ret + ")");
        }
    }
}