﻿using System;
using up6.db.biz;
using up6.utils;

namespace up6.api.up6.nosql
{
    public partial class f_del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string fid = this.reqString("id");
            string uid = this.reqString("uid");
            string callback = this.reqStringSafe("callback");
            int ret = 0;

            if (string.IsNullOrEmpty(fid) ||
                string.IsNullOrEmpty(uid)
                )
            {
            }//参数不为空
            else
            {
                up6_biz_event.file_del(fid, uid);
                ret = 1;
            }
            this.toContentJson(callback + "(" + ret + ")");//返回jsonp格式数据
        }
    }
}