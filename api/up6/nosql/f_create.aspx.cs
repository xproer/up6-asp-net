﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Web;
using up6.db.biz;
using up6.db.model;
using up6.db.utils;
using up6.utils;

namespace up6.api.up6.nosql
{
    public partial class f_create : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string op = this.reqString("op");
            string pid = this.reqString("pid");
            string pidRoot = this.reqString("pidRoot");
            string md5 = this.reqString("md5");
            string id = this.reqString("id");
            string uid = this.reqString("uid");
            string sizeLoc = this.reqString("sizeLoc");
            string blockSize = this.reqString("blockSize", "5242880");
            string token = this.reqString("token");
            string callback = this.reqString("callback");//jsonp参数
            //客户端使用的是encodeURIComponent编码，
            string pathLoc = this.reqStringDecode("pathLoc");//utf-8解码
            pathLoc = pathLoc.Replace("\\", "/");

            if (string.IsNullOrEmpty(pid)) pid = string.Empty;
            if (string.IsNullOrEmpty(pidRoot)) pidRoot = pid;

            //参数为空
            if (string.IsNullOrEmpty(md5) ||
                string.IsNullOrEmpty(uid) ||
                string.IsNullOrEmpty(sizeLoc)
                )
            {
                Response.Write(callback + "({\"value\":null})");
                return;
            }

            FileInf fileSvr = new FileInf();
            fileSvr.fdChild = false;
            fileSvr.uid = uid;//将当前文件UID设置为当前用户UID
            fileSvr.id = id;
            fileSvr.pid = pid;
            fileSvr.fdChild = !string.IsNullOrEmpty(pid);
            fileSvr.pidRoot = pidRoot;
            fileSvr.nameLoc = Path.GetFileName(pathLoc);
            fileSvr.pathLoc = pathLoc;
            fileSvr.lenLoc = this.reqLong("lenLoc");
            fileSvr.lenLocSec = this.reqLong("lenLocSec");
            fileSvr.sizeLoc = sizeLoc;
            fileSvr.deleted = false;
            fileSvr.md5 = md5;
            fileSvr.nameSvr = fileSvr.nameLoc;
            fileSvr.blockSize = int.Parse(blockSize);//块大小
            fileSvr.blockSizeSec = this.reqInt("blockSizeSec");//块国加密大小
            fileSvr.encrypt = ConfigReader.storageEncrypt();//存储加密
            fileSvr.encryptAgo = this.reqString("encryptAgo");//加密算法

            WebSafe ws = new WebSafe();
            var ret = ws.validToken(token, fileSvr);
            //token验证失败
            if (!ret)
            {
                string m = callback + "({\"value\":\"0\",\"ret\":false,\"msg\":\"token error\"})";//返回jsonp格式数据。
                this.toContentJson(m);
                return;
            }

            //所有单个文件均以uuid/file方式存储
            PathBuilderUuid pb = new PathBuilderUuid();
            fileSvr.pathSvr = pb.genFile(fileSvr.uid, ref fileSvr);
            fileSvr.pathSvr = fileSvr.pathSvr.Replace("\\", "/");

            try
            {
                //创建文件
                var fw = ConfigReader.blockWriter();
                fileSvr.object_id = fw.make(fileSvr);
                fileSvr.object_key = fileSvr.getObjectKey();
            }
            catch (IOException ie)
            {
                var obj = new JObject { { "value", "" }, { "error", ie.Message }, { "ret", false } };
                this.toContentJson(callback + "(" + obj.ToString() + ")");
                return;
            }

            //触发事件
            up6_biz_event.file_create(fileSvr);

            //将路径转换成相对路径
            fileSvr.pathSvr = pb.absToRel(fileSvr.pathSvr);

            string jv = JsonConvert.SerializeObject(fileSvr);
            jv = HttpUtility.UrlEncode(jv);
            jv = jv.Replace("+", "%20");
            string json = callback + "({\"value\":\"" + jv + "\",\"ret\":true})";//返回jsonp格式数据。
            this.toContentJson(json);
    }
    }
}