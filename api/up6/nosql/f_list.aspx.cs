﻿using System;
using up6.utils;

namespace up6.api.up6.nosql
{
    public partial class f_list : WebBase
    {
        /// <summary>
        /// 以JSON格式列出所有文件（）
        /// 注意，输出的文件路径会进行UrlEncode编码
        /// 客户端需要进行UrlDecode解码
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            string uid = this.reqString("uid");
            string cbk = this.reqString("callback");//jsonp

            this.toContentJson(cbk + "({\"value\":null})");
        }
    }
}