﻿using System;
using up6.db.database.up6.sql;
using up6.utils;

namespace api.up6
{
    public partial class fd_del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fid = this.reqString("id");
            string uid = this.reqString("uid");
            string cbk = this.reqString("callback");
            int ret = 0;

            if (string.IsNullOrEmpty(fid) )
            {
            }//参数不为空
            else
            {
                SqlFolder.build().del(fid, uid);
                ret = 1;
            }
            this.toContentJson(cbk + "({\"value\":" + ret + "})");//返回jsonp格式数据
        }
    }
}