﻿using System;
using up6.db.biz;
using up6.db.database.up6.sql;
using up6.utils;

namespace api.up6
{
    public partial class f_del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fid = this.reqString("id");
            string uid = this.reqString("uid");
            string callback = this.reqStringSafe("callback");
            int ret = 0;

            if (string.IsNullOrEmpty(fid))
            {
            }//参数不为空
            else
            {
                SqlFile.build().Delete(uid, fid);
                up6_biz_event.file_del(fid,uid);
                ret = 1;
            }
            this.toContentJson(callback + "(" + ret + ")");//返回jsonp格式数据
        }
    }
}