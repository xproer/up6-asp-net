﻿using System;
using up6.db.biz;
using up6.db.database.up6.sql;
using up6.utils;

namespace api.up6
{
    /// <summary>
    /// 此文件处理单文件上传
    /// </summary>
    public partial class f_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var md5 = this.reqString("md5");
            var uid = this.reqString("uid");
            var id = this.reqString("id");
            var pid = this.reqString("pid");
            var cbk = this.reqStringSafe("callback");
            var nameLoc = this.reqStringDecode("nameLoc");//文件名称

            //返回值。1表示成功
            int ret = 0;

            if (string.IsNullOrEmpty(id) )
            {
            }//参数不为空
            else
            {
                SqlFile.build().complete(id);

                up6_biz_event.file_post_complete(id);
                ret = 1;
            }
            this.toContentJson(cbk + "(" + ret + ")");//必须返回jsonp格式数据
        }
    }
}