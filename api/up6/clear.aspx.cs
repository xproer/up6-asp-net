﻿using System;
using up6.db.database;

namespace api.up6
{
    public partial class clear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DBConfig cfg = new DBConfig();
            global::up6.db.database.up6.sql.SqlFile db = cfg.file();
            db.Clear();
        }
    }
}