﻿using Newtonsoft.Json.Linq;
using System;
using up6.db.model;
using up6.db.sql;
using up6.db.utils;
using up6.filemgr.app;
using up6.utils;

namespace up6.api.filemgr
{
    public partial class mkdir : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var obj = this.reqJson();
            var f = obj.ToObject<FileInf>();
            bool isRootDir = f.pathRel== "/";
            f.pathRel = PathTool.combin(f.pathRel, f.nameLoc);

            DbFolder df = new DbFolder();
            if (df.exist_same_folder(f.pathRel))
            {
                var ret = new JObject { { "ret", false }, { "msg", "已存在同名目录" } };
                this.toContent(ret);
                return;
            }

            f.id = Guid.NewGuid().ToString("N");
            f.complete = true;
            f.fdTask = true;

            //根目录
            if (isRootDir)
            {
                SqlTable.build("up6_files").insert(f);
            }//子目录
            else
            {
                SqlTable.build("up6_folders").insert(f);
            }

            obj = JObject.FromObject(f);
            obj["ret"] = true;
            this.toContent(obj);
        }
    }
}