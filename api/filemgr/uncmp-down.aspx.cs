﻿using Newtonsoft.Json.Linq;
using System;
using up6.db.model;
using up6.db.sql;
using up6.utils;

namespace up6.api.filemgr
{
    /// <summary>
    /// 更新记录：
    ///     2022-12-16 使用新的API
    /// </summary>
    public partial class uncmp_down : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string uid = this.reqString("uid");
            var fs = SqlTable.build("down_files").reads<DnFileInf>(
                "f_id,f_nameLoc,f_pathLoc,f_perLoc,f_sizeSvr,f_fdTask",
                SqlWhere.build().eq("f_uid", uid),
                SqlSort.build());
            
            this.toContentJson( JToken.FromObject(fs));
        }
    }
}