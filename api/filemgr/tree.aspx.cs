﻿using Newtonsoft.Json.Linq;
using System;
using up6.db.database;
using up6.sql;
using up6.utils;

namespace up6.api.filemgr
{
    public partial class tree : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var pid = Request.QueryString["pid"];
            var swm = new SqlWhereMerge();
            swm.equal("f_fdChild", 0);
            swm.equal("f_fdTask", 1);
            swm.equal("f_deleted", 0);
            if (!string.IsNullOrEmpty(pid)) swm.equal("f_pid", pid);

            DBConfig cfg = new DBConfig();
            SqlExec se = cfg.se();
            JArray arr = new JArray();
            var data = se.select("up6_files"
                , "f_id,f_pid,f_pidRoot,f_nameLoc"
                , swm.to_sql()
                , string.Empty);

            //查子目录
            if (!string.IsNullOrEmpty(pid))
            {
                data = se.select("up6_folders"
                    , "f_id,f_pid,f_pidRoot,f_nameLoc"
                    , new SqlParam[] {
                        new SqlParam("f_pid", pid)
                        ,new SqlParam("f_deleted", false)
                    });
            }

            foreach (var f in data)
            {
                var item = new JObject();
                item["id"] = f["f_id"].ToString();
                item["text"] = f["f_nameLoc"].ToString();
                item["parent"] = "#";
                item["nodeSvr"] = f;
                arr.Add(item);
            }
            this.toContent(arr);
        }
    }
}