﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using up6.db.model;
using up6.db.sql;
using up6.utils;

namespace up6.api.filemgr
{
    /// <summary>
    /// 优化逻辑
    /// </summary>
    public partial class api : WebBase
    {
        string m_uid = "";
        string m_pathRelCds = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.m_uid = this.reqString("uid");
            var pathRel = this.reqStringDecode("pathRel");
            if(!pathRel.EndsWith("/")) pathRel += "/";
            this.m_pathRelCds = string.Format("f_pathRel='{0}'+f_nameLoc", pathRel);

            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.Kingbase || tp == DataBaseType.ODBC || tp == DataBaseType.Oracle)
            {
                this.m_pathRelCds = string.Format("f_pathRel=CONCAT('{0}',f_nameLoc)", pathRel);
            }
            if (pathRel=="/") this.loadDataRoot();
            else this.loadDataChild();
        }

        /// <summary>
        /// 加载根目录下的文件和目录
        /// </summary>
        void loadDataRoot() {
            var fs = SqlTable.build("up6_files").reads<FileInf>(
                "f_id,f_pid,f_nameLoc,f_sizeLoc,f_lenLoc,f_time,f_pidRoot,f_fdTask,f_pathSvr,f_pathRel,f_lenLocSec,f_encrypt,f_encryptAgo,f_blockSize,f_blockSizeSec,f_object_key",
                SqlWhere.build()
                .eq("f_complete", true)
                .eq("f_uid", this.reqString("uid"))
                .eq("f_deleted", false)
                .eq("f_fdChild", false)
                .sql("f_pathRel", this.m_pathRelCds),
                SqlSort.build().desc("f_fdTask").desc("f_time")
                );

            JObject o = new JObject();
            o["count"] = fs.Count();
            o["code"] = 0;
            o["msg"] = string.Empty;
            o["data"] = JArray.FromObject(fs);

            this.toContentJson(o);
        }

        /// <summary>
        /// 加载子目录数据,包含子文件和子目录
        /// </summary>
        void loadDataChild()
        {
            var files = SqlTable.build("up6_files").reads<FileInf>(
                "f_id,f_pid,f_nameLoc,f_sizeLoc,f_lenLoc,f_time,f_pidRoot,f_fdTask,f_pathSvr,f_pathRel,f_lenLocSec,f_encrypt,f_encryptAgo,f_blockSize,f_blockSizeSec,f_object_key",
                SqlWhere.build()
                .eq("f_uid", this.reqString("uid"))
                .eq("f_complete", true)
                .eq("f_deleted", false)
                .eq("f_fdChild", true)
                .sql("f_pathRel", this.m_pathRelCds),
                SqlSort.build()
                .desc("f_fdTask")
                .desc("f_time").asc("f_nameLoc"));

            var folders = SqlTable.build("up6_folders").reads<FileInf>(
                "f_id,f_nameLoc,f_pid,f_sizeLoc,f_time,f_pidRoot,f_pathRel",
                SqlWhere.build()
                .eq("f_complete", true)
                .eq("f_deleted", false)
                .eq("f_uid", this.reqString("uid"))
                .sql("f_pathRel", this.m_pathRelCds),
                SqlSort.build()
                .desc("f_time")
                .asc("f_nameLoc"));

            foreach (var fd in folders)
            {
                fd.fdTask = true;
                fd.fdChild = false;
                fd.pathSvr = string.Empty;
                files.Insert(0, fd);
            }

            JObject o = new JObject();
            o["count"] = files.Count();
            o["code"] = 0;
            o["msg"] = string.Empty;
            o["data"] = JArray.FromObject(files);

            this.toContentJson(o);
        }
    }
}