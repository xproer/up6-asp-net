﻿using Newtonsoft.Json.Linq;
using System;
using up6.db.database;
using up6.sql;
using up6.utils;

namespace up6.api.filemgr
{
    public partial class del_batch : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var par = Request.Form["data"];
            par = Server.UrlDecode(par);
            var obj = JToken.Parse(par);

            DBConfig cfg = new DBConfig();
            SqlExec se = cfg.se();

            //更新文件
            se.exec_batch("up6_files"
                , "update up6_files set f_deleted=1 where f_id=@f_id"
                , string.Empty
                , "f_id"
                , obj);

            //更新文件夹
            se.exec_batch("up6_folders"
                , "update up6_folders set f_deleted=1 where f_id=@f_id"
                , string.Empty
                , "f_id"
                , obj);

            this.toContent(obj);
        }
    }
}