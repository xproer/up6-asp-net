﻿using Newtonsoft.Json.Linq;
using System;
using up6.filemgr.app;
using up6.utils;

namespace up6.api.filemgr
{
    /// <summary>
    /// 生成导航路径
    /// </summary>
    public partial class path : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //fields,FileInf
            var data = this.reqString("data");
            data = Server.UrlDecode(data);
            var fd = JObject.Parse(data);

            DbFolder df = new DbFolder();
            var d = df.build_path(fd);

            this.toContentJson(JToken.FromObject(d));
        }
    }
}