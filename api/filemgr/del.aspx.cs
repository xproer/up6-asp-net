﻿using Newtonsoft.Json.Linq;
using System;
using up6.db.database;
using up6.sql;
using up6.utils;

namespace up6.api.filemgr
{
    /// <summary>
    /// 删除文件
    /// </summary>
    public partial class del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = this.reqString("id");
            var pathRel = this.reqStringDecode("pathRel");
            pathRel += '/';

            SqlWhereMerge swm = new SqlWhereMerge();
            DBConfig cfg = new DBConfig();
            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.Kingbase || tp == DataBaseType.ODBC || tp == DataBaseType.Oracle)
            {
                swm.instr(pathRel, "f_pathRel");
            }
            else
            {
                swm.charindex(pathRel, "f_pathRel");
            }
            string where = swm.to_sql();

            SqlExec se = cfg.se();
            se.update("up6_folders"
                , new SqlParam[] { new SqlParam("f_deleted", true) }
                , where
                );

            se.update("up6_folders"
                , new SqlParam[] { new SqlParam("f_deleted", true) }
                , new SqlParam[] { new SqlParam("f_id", id) }
                );

            se.update("up6_files"
                , new SqlParam[] { new SqlParam("f_deleted", true) }
                , where
                );

            se.update("up6_files"
                , new SqlParam[] { new SqlParam("f_deleted", true) }
                , new SqlParam[] { new SqlParam("f_id", id) }
                );

            this.toContent(new JObject { { "ret", 1 } });
        }
    }
}