﻿using Newtonsoft.Json;
using System;
using up6.filemgr.app;
using up6.utils;

namespace up6.api.filemgr
{
    /// 获取文件夹结构（JSON）
    /// 格式：
    /// [
    ///   {nameLoc,pathSvr,pathRel,lenSvr,sizeSvr}
    ///   {nameLoc,pathSvr,pathRel,lenSvr,sizeSvr}
    /// ]
    public partial class fd_data : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqString("id");
            string pid = this.reqString("pid");
            string cbk = this.reqString("callback");
            string json = "({\"value\":null})";

            if (!string.IsNullOrEmpty(id))
            {
                FolderBuilder fb = new FolderBuilder();
                var data = JsonConvert.SerializeObject(fb.build(id,pid));
                data = this.Server.UrlEncode(data);
                data = data.Replace("+", "%20");

                json = "({\"value\":\"" + data + "\"})";
            }
            this.toContent(cbk + json);

        }
    }
}