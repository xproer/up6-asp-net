﻿using Newtonsoft.Json.Linq;
using System;
using up6.db.model;
using up6.db.sql;
using up6.utils;

namespace up6.api.filemgr
{
    /// <summary>
    /// 加载未完成的文件和目录列表
    /// 更新记录：
    ///     2022-12-16 使用新的API
    /// </summary>
    public partial class uncomp : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string uid = this.reqString("uid");
            var files = SqlTable.build("up6_files").reads<FileInf>(
                "f_id ,f_nameLoc ,f_pathLoc ,f_sizeLoc ,f_lenSvr ,f_perSvr ,f_fdTask ,f_md5 ",
                SqlWhere.build()
                .eq("f_complete", false)
                .eq("f_deleted", false)
                .eq("f_uid", uid),
                SqlSort.build());

            this.toContentJson( JToken.FromObject(files) );
        }
    }
}