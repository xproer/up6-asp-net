﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Web;
using up6.db.biz;
using up6.db.database.up6.sql;
using up6.db.model;
using up6.db.utils;
using up6.utils;

namespace up6.api.filemgr
{
    public partial class f_create : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string pid = this.reqString("pid");
            string pidRoot = this.reqString("pidRoot");
            string md5 = this.reqString("md5");
            string id = this.reqString("id");
            string uid = this.reqString("uid");
            string lenLoc = this.reqString("lenLoc");
            string sizeLoc = this.reqString("sizeLoc");
            string blockSize = this.reqString("blockSize", "5242880");
            string encryptAgo = this.reqString("encryptAgo");
            var blockSizeSec = this.reqInt("blockSizeSec");
            string callback = this.reqString("callback");//jsonp参数
            //客户端使用的是encodeURIComponent编码，
            string pathLoc = this.reqStringDecode("pathLoc");//utf-8解码
            string pathRel = this.reqStringDecode("pathRel");

            if (string.IsNullOrEmpty(pid)) pid = string.Empty;
            if (string.IsNullOrEmpty(pidRoot)) pidRoot = pid;

            //参数为空
            if (string.IsNullOrEmpty(md5)
                || string.IsNullOrEmpty(uid)
                || string.IsNullOrEmpty(sizeLoc))
            {
                Response.Write(callback + "({\"value\":null})");
                return;
            }

            FileInf fileSvr = new FileInf();
            fileSvr.fdChild = false;
            fileSvr.uid = uid;//将当前文件UID设置为当前用户UID
            fileSvr.id = id;
            fileSvr.pid = pid;
            bool isRootFile = pathRel == "/";
            fileSvr.fdChild = !isRootFile;
            fileSvr.pidRoot = pidRoot;
            fileSvr.nameLoc = Path.GetFileName(pathLoc);
            fileSvr.pathLoc = pathLoc;
            fileSvr.pathRel = PathTool.combin(pathRel, fileSvr.nameLoc);
            fileSvr.lenLoc = Convert.ToInt64(lenLoc);
            fileSvr.lenLocSec = this.reqLong("lenLocSec");
            fileSvr.sizeLoc = sizeLoc;
            fileSvr.deleted = false;
            fileSvr.md5 = md5;
            fileSvr.nameSvr = fileSvr.nameLoc;
            fileSvr.blockSize = int.Parse(blockSize);//块大小
            fileSvr.blockSizeSec = blockSizeSec;//加密块大小
            fileSvr.encrypt = ConfigReader.storageEncrypt();//存储加密
            fileSvr.encryptAgo = encryptAgo;//加密算法

            //同名文件检测
            //DbFolder df = new DbFolder();
            //if (df.exist_same_file(fileSvr.nameLoc, pid))
            //{
            //    var data = callback + "({'value':'','ret':false,'code':'101'})";
            //    this.toContent(data);
            //    return;
            //}

            //所有单个文件均以uuid/file方式存储
            PathBuilderUuid pb = new PathBuilderUuid();
            fileSvr.pathSvr = pb.genFile(fileSvr.uid, ref fileSvr);
            fileSvr.pathSvr = fileSvr.pathSvr.Replace("\\", "/");

            //数据库存在相同文件
            var dbFile = SqlFile.build();
            FileInf fileExist = dbFile.exist_file(md5);
            if (null!=fileExist)
            {
                fileSvr.nameSvr = fileExist.nameSvr;
                fileSvr.pathSvr = fileExist.pathSvr;
                fileSvr.perSvr = fileExist.perSvr;
                fileSvr.lenSvr = fileExist.lenSvr;
                fileSvr.complete = fileExist.complete;
                fileSvr.lenLocSec = fileExist.lenLocSec;
                fileSvr.encryptAgo = fileExist.encryptAgo;
                fileSvr.encrypt = fileExist.encrypt;
                fileSvr.blockSize = fileExist.blockSize;
                fileSvr.blockSizeSec = fileExist.blockSizeSec;
                fileSvr.object_key = fileExist.object_key;
                dbFile.Add(ref fileSvr);

                //触发事件
                up6_biz_event.file_create_same(fileSvr);
            }//数据库不存在相同文件
            else
            {
                //2.0创建器。仅创建一个空白文件
                var fw = ConfigReader.blockWriter();
                fileSvr.object_id = fw.make(fileSvr);
                fileSvr.object_key = fileSvr.getObjectKey();

                dbFile.Add(ref fileSvr);
                //触发事件
                up6_biz_event.file_create(fileSvr);
            }

            string jv = JsonConvert.SerializeObject(fileSvr);
            jv = HttpUtility.UrlEncode(jv);
            jv = jv.Replace("+", "%20");
            string json = callback + "({\"value\":\"" + jv + "\",\"ret\":true})";//返回jsonp格式数据。
            this.toContent(json);
        }
    }
}