﻿using System;
using up6.db.biz;
using up6.db.database.up6.sql;
using up6.db.filemgr;
using up6.db.model;
using up6.utils;

namespace up6.api.filemgr
{
    /// <summary>
    /// 更新记录：
    ///   2022-12-11 
    ///     取消单独设置子文件和目录状态操作
    ///     修复读取同名同路径目录错误的问题。
    /// </summary>
    public partial class fd_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqString("id");
            string pid = this.reqString("pid");
            string pathRel = this.reqStringDecode("pathRel");
            var uid = this.reqString("uid");
            string cak = this.reqString("callback");
            int cover = this.reqInt("cover");
            int ret = 0;

            if (string.IsNullOrEmpty(id))
            {
            }
            else
            {
                FileInf folder = null;
                FileInf fdExist = null;

                //当前目录是根目录?
                bool isRootDir = pathRel=="/";
                //根目录
                if (isRootDir)
                {
                    folder = SqlFile.build().read(id);
                    //查询同级同名目录
                    fdExist = SqlFile.build().read(folder.pathRel, id,folder.uid);

                    //存在相同目录=>直接使用已存在的目录信息=>删除旧目录
                    if (fdExist != null) SqlFile.build().Delete(uid, fdExist.id);
                }
                //子目录
                else
                {
                    folder = SqlFolder.build().read(id);
                    folder.uid = uid;
                    fdExist = SqlFolder.build().read(folder.pathRel, id,folder.uid);

                    //存在相同目录=>直接使用已存在的目录信息=>删除旧目录
                    if (fdExist != null) SqlFolder.build().del(fdExist.id, uid);
                }

                //存在同名，同路径文件
                if (fdExist != null)
                {
                    folder.id = fdExist.id;
                    folder.pid = fdExist.pid;
                    folder.pidRoot = fdExist.pidRoot;                    
                }

                //根节点
                FileInf root = new FileInf();
                root.id = folder.pidRoot;
                root.uid = folder.uid;
                //当前节点是根节点
                if (string.IsNullOrEmpty(root.id)) root.id = folder.id;

                //保存层级结构-解析层级信息文件
                FolderScaner fsd = new FolderScaner();
                fsd.m_cover = fdExist != null;
                fsd.save(folder);

                //上传完毕=>子目录
                if (!isRootDir) SqlFolder.build().complete(id,uid);
                //根目录
                else SqlFile.build().complete(id);

                up6_biz_event.folder_post_complete(id);

                ret = 1;
            }
            this.toContentJson(cak + "(" + ret + ")");
        }
    }
}