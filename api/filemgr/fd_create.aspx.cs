﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Web;
using up6.db.biz;
using up6.db.model;
using up6.db.utils;
using up6.utils;
using up6.db.database.up6.sql;

namespace up6.api.filemgr
{
    public partial class fd_create : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqString("id");
            string pid = this.reqString("pid");
            string uid = this.reqString("uid");
            string lenLoc = this.reqString("lenLoc");
            string sizeLoc = this.reqString("sizeLoc");
            string pathLoc = this.reqStringDecode("pathLoc");
            string pathRel = this.reqStringDecode("pathRel");
            string callback = this.reqString("callback");//jsonp参数
            if (string.IsNullOrEmpty(pid)) pid = string.Empty;
            pid = pid.Trim();

            if (string.IsNullOrEmpty(id)
                || string.IsNullOrEmpty(uid)
                || string.IsNullOrEmpty(pathLoc)
                )
            {
                this.toContentJson(callback + "({\"value\":null})");
                return;
            }

            FileInf fileSvr = new FileInf();
            fileSvr.id = id;
            fileSvr.pid = pid;
            fileSvr.pidRoot = "";
            fileSvr.fdChild = false;
            fileSvr.fdTask = true;
            fileSvr.uid = uid;//将当前文件UID设置为当前用户UID
            fileSvr.nameLoc = Path.GetFileName(pathLoc);
            fileSvr.pathLoc = pathLoc;
            fileSvr.pathRel = PathTool.combin(pathRel, fileSvr.nameLoc);
            fileSvr.lenLoc = Convert.ToInt64(lenLoc);
            fileSvr.sizeLoc = sizeLoc;
            fileSvr.deleted = false;
            fileSvr.nameSvr = fileSvr.nameLoc;

            //检查同名目录
            //DbFolder df = new DbFolder();
            //if (df.exist_same_folder(fileSvr.nameLoc, pid))
            //{
            //    var o = new JObject { { "value", null }, { "ret", false }, { "code", "102" } };
            //    var js = callback + string.Format("({0})", JsonConvert.SerializeObject(o));
            //    this.toContent(js);
            //    return;
            //}

            //生成存储路径
            PathBuilderUuid pb = new PathBuilderUuid();
            fileSvr.pathSvr = pb.genFolder(ref fileSvr);
            fileSvr.pathSvr = fileSvr.pathSvr.Replace("\\", "/");
            if (!Directory.Exists(fileSvr.pathSvr)) Directory.CreateDirectory(fileSvr.pathSvr);

            //保存层级结构
            FolderSchema fs = new FolderSchema();
            fs.create(fileSvr);

            //添加成根目录
            if (pathRel=="/")
            {
                db.database.up6.sql.SqlFile.build().Add(ref fileSvr);
            }//添加成子目录
            else
            {
                SqlFolder.build().add(fileSvr);
            }

            up6_biz_event.folder_create(fileSvr);

            string json = JsonConvert.SerializeObject(fileSvr);
            json = HttpUtility.UrlEncode(json);
            json = json.Replace("+", "%20");
            var jo = new JObject { { "value", json }, { "ret", true } };
            json = callback + string.Format("({0})", JsonConvert.SerializeObject(jo));
            this.toContent(json);
        }
    }
}